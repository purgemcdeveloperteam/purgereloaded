package com.gmail.mcraftworldmc.thepurge.Listener;

import java.util.Arrays;
import java.util.Collections;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerLoginEvent.Result;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.server.ServerListPingEvent;

import com.gmail.mcraftworldmc.thepurge.Main;
import com.gmail.mcraftworldmc.thepurge.utilities.GameState;
import com.gmail.mcraftworldmc.thepurge.utilities.LocationParser;
import com.gmail.mcraftworldmc.thepurge.utilities.Misc;

public class PurgeMainListener implements Listener{
	private Main plugin;
	public PurgeMainListener(Main instance){
		plugin = instance;
	}
	@EventHandler
	public void onJoin(PlayerJoinEvent event){
		final Player p = event.getPlayer();
		event.setJoinMessage(Misc.determineNameColor(p) + ChatColor.WHITE + " has joined.");
		p.setHealth(20D);
		p.setFoodLevel(20);
		p.getInventory().clear();
		p.getInventory().setArmorContents(null);
		if(plugin.maps.getMapsConfig().getString("lobby").isEmpty()){
			p.sendMessage(plugin.prefix + "The default Purge lobby has not been set! It has been set for this world: " + ChatColor.GREEN + event.getPlayer().getWorld().getName());
			p.sendMessage(ChatColor.YELLOW + "At this location: " + p.getLocation().getX() + " " + p.getLocation().getY() + " " + p.getLocation().getZ());
			p.sendMessage(ChatColor.YELLOW + "To set a different lobby location, please refer to the /purge command, Thank you!");
			plugin.maps.getMapsConfig().set("lobby", LocationParser.parseLocationToStringWithYP(p.getLocation()));
			plugin.maps.saveMapsConfig();
			return;
		}
		if(!plugin.game.getGameState().equals(GameState.LOBBY)){
			return;
		}
		Bukkit.getScheduler().runTaskLater(plugin, new Runnable(){
			public void run(){
				p.teleport(LocationParser.parseLocationYP(plugin.maps.getMapsConfig().getString("lobby")));
				p.sendMessage(plugin.prefix + "Welcome to the annual purge! All emergency services will be suspended for a 12-hour period!");
			}
		}, 2l);
		if(!p.isOp() && !p.getGameMode().equals(GameMode.ADVENTURE)){
			p.setAllowFlight(false);
		}
		if(Bukkit.getOnlinePlayers().length == 1 && plugin.getConfig().getString("purge.setupmode").equalsIgnoreCase("false") && plugin.game.getGameState().equals(GameState.LOBBY)){
			plugin.preTimer.start();
		}
		for(Player pl : Bukkit.getOnlinePlayers()){
			pl.showPlayer(p);
		}
	}
	@EventHandler
	public void onQuit(PlayerQuitEvent e){
		if(Bukkit.getOnlinePlayers().length - 1 <= 0){
			try{
				plugin.preTimer.stop();
			    plugin.timer.stop();
			}catch(IllegalStateException ex){
				if(plugin.getConfig().getString("purge.devTool").equalsIgnoreCase("true")){
					plugin.getLogger().info("Supressed IllegalStateException: " + ex.getMessage());
				}
			}
		}
		e.setQuitMessage(Misc.determineNameColor(e.getPlayer()) + ChatColor.WHITE + " has left.");
	}
	@EventHandler
	public void onPing(ServerListPingEvent e){
		if(plugin.getConfig().getBoolean("purge.setupmode") == true){
			return;
		}
		if(plugin.game.getGameState().equals(GameState.LOBBY)){
			e.setMotd("Lobby");
		}
		if(plugin.game.getGameState().equals(GameState.GAME)){
			e.setMotd("In Progress!");
		}
		if(plugin.game.getGameState().equals(GameState.AFTER_GAME) || plugin.game.getGameState().equals(GameState.RESTARTING)){
			e.setMotd("RESTARTING");
			e.setMaxPlayers(1);
		}
	}
	@EventHandler
	public void onPlace(BlockPlaceEvent e){
		if(!e.getPlayer().isOp())
			e.setCancelled(true);
	}
	@EventHandler
	public void onHungerChange(FoodLevelChangeEvent e){
		if(plugin.game.getGameState().equals(GameState.LOBBY)){
			e.setCancelled(true);
			e.setFoodLevel(20);
		}
		if(plugin.game.getSpectatorList().contains(e.getEntity().getName())){
			e.setCancelled(true);
		}
	}
	@EventHandler
	public void onChat(AsyncPlayerChatEvent e){
		if(!plugin.game.getGameState().equals(GameState.LOBBY) && !plugin.game.getGameState().equals(GameState.AFTER_GAME))
			return;
		e.setCancelled(true);
		Bukkit.broadcastMessage(Misc.determineNameColor(e.getPlayer()) + ChatColor.DARK_GRAY + ": " + (Misc.isStaff(e.getPlayer()) ? ChatColor.AQUA : ChatColor.WHITE) + e.getMessage());
	}
	@EventHandler
	public void onLogin(PlayerLoginEvent e){
		Player p = e.getPlayer();
		if(!plugin.game.getGameState().equals(GameState.LOBBY))
			return;
		if(e.getResult().equals(Result.KICK_FULL)){
			if(Misc.isDonor(p)){
				Player[] pl = Bukkit.getOnlinePlayers();
				Collections.shuffle(Arrays.asList(pl));
				for(Player po : pl){
					if(!Misc.isDonor(po)){
						po.kickPlayer(ChatColor.GREEN + "You have been kicked to make room for a donor.");
						e.allow();
						e.setResult(Result.ALLOWED);
						return;
					}
				}
				if(!p.hasPermission("purge.gold") && !Misc.isStaff(p)){
					for(Player po : pl){
						if(po.hasPermission("purge.gold") && !Misc.isStaff(po)){
							po.kickPlayer(ChatColor.GREEN + "You have been kicked to make room for a donor.");
							e.allow();
							e.setResult(Result.ALLOWED);
							return;
						}
					}
				}
				if(!p.hasPermission("purge.diamond") && !Misc.isStaff(p) && !p.hasPermission("purge.gold")){
					for(Player po : pl){
						if(po.hasPermission("purge.diamond") && !Misc.isStaff(po)){
							po.kickPlayer(ChatColor.GREEN + "You have been kicked to make room for a donor.");
							e.allow();
							e.setResult(Result.ALLOWED);
						}
					}
				}
				if(Misc.isStaff(p)){
					for(Player po : pl){
						if(Misc.isStaff(po))
							continue;
						po.kickPlayer(ChatColor.GREEN + "You have been kicked to make room for a donor.");
						e.allow();
						e.setResult(Result.ALLOWED);
						return;
					}
				}
			}
		}
	}
}
