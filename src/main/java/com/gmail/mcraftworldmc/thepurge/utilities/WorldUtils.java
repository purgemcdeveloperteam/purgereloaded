package com.gmail.mcraftworldmc.thepurge.utilities;

import org.bukkit.Bukkit;
import org.bukkit.WorldCreator;

public class WorldUtils {
	public static void load(String n){
		Bukkit.createWorld(new WorldCreator(n)).setAutoSave(false);
	}
	public static void unload(String n, boolean b){
		if(Bukkit.unloadWorld(n, b)){
			Bukkit.getLogger().info("Successfully unloaded world: " + n);
		}else{
			Bukkit.getLogger().info("Successfully unloaded world: " + n);
		}
	}
}
