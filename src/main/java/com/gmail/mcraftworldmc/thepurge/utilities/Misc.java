package com.gmail.mcraftworldmc.thepurge.utilities;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import com.gmail.mcraftworldmc.thepurge.Main;

public class Misc {
	public static boolean isStaff(Player p){
		return (p.hasPermission("purge.admin")) || (p.hasPermission("purge.mod")) || (p.hasPermission("purge.dev"));
	}
	public static String determineNameColor(Player p){
		if(p.hasPermission("purge.admin")){
			return ChatColor.DARK_RED + p.getName();
		}else if(p.hasPermission("purge.mod")){
			return ChatColor.RED + p.getName();
		}else if(p.hasPermission("purge.emerald")){
			return ChatColor.GREEN + p.getName();
		}else if(p.hasPermission("purge.diamond")){
			return ChatColor.DARK_AQUA + p.getName();
		}else if(p.hasPermission("purge.gold")){
			return ChatColor.GOLD + p.getName();
		}else if(p.hasPermission("purge.vip")){
			return ChatColor.DARK_PURPLE + p.getName();
		}else if(p.hasPermission("purge.dev")){
			return ChatColor.LIGHT_PURPLE + p.getName();
		}
		else{
			return ChatColor.GRAY + p.getName();
		}
	}
	public static void connectToServer(Player p){
		ByteArrayOutputStream bo = new ByteArrayOutputStream();
		DataOutputStream d = new DataOutputStream(bo);
		try{
			d.writeUTF("Connect");
			d.writeUTF("hub");
		}catch(Exception e){
			Main.getInstance().getLogger().severe("Could not connect to server!");
			e.printStackTrace();
		}
		p.sendPluginMessage(Main.getInstance(), "BungeeCord", bo.toByteArray());
	}
	public static boolean isDonor(Player p){
		return (p.hasPermission("purge.gold")) || (p.hasPermission("purge.diamond")) || (p.hasPermission("purge.emerald")) || (p.hasPermission("purge.vip")) || isStaff(p);
	}
}
