package com.gmail.mcraftworldmc.thepurge.timers;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;

import com.gmail.mcraftworldmc.thepurge.Main;

@SuppressWarnings("deprecation")
public class GameTimer extends BukkitRunnable{
	private Main plugin;
	public GameTimer(Main instance){
		plugin = instance;
	}
	private int timeInSeconds;
	private int origin;
	private int taskID;
	private int c = 0;
	@SuppressWarnings("unused")
	private int extraTime;
	private boolean extraTimeActive = false;
	public void start(){
		stop();
		taskID = Bukkit.getScheduler().scheduleSyncRepeatingTask(plugin, this, 0L, 20L);
		resetTime();
		if(timeInSeconds > 600){
			extraTime = timeInSeconds - 600;
			extraTimeActive = true;
		}
	}
	public void pause(){
		this.cancel();
	}
	public void resume(){
		taskID = Bukkit.getScheduler().scheduleSyncRepeatingTask(plugin, this, 0L, 20L);
	}
	public void run(){
		if(timeInSeconds > 0){
			timeInSeconds--;
			int minutes = timeInSeconds / 60;
			int seconds = timeInSeconds % 60;
			String str = String.format("%d:%02d", minutes, seconds);
			if(plugin.getConfig().getBoolean("purge.devTool")){
				plugin.getLogger().info(timeInSeconds + "");
			}
			for(Player p : Bukkit.getOnlinePlayers()){
				try{
					if(c == 0)
						plugin.board.setGameBoard(p, str, plugin.game.getSyndicateCount(), plugin.game.getCivillianCount(), plugin.game.getSyndicateList(), plugin.game.getCivillianList(), plugin.game.getSpectatorList(), plugin.game.getTarget());
					else
						plugin.board.updateGameboard(str, plugin.game.getSyndicateCount(), plugin.game.getCivillianCount(), plugin.game.getSpectatorList().size());
				}catch(NullPointerException ex){
					if(plugin.getConfig().getBoolean("purge.devTool")){
						plugin.getLogger().info("Suppressed NullPointerException: " + ex.getMessage());
					}
				}
			}
			for(String s : plugin.game.getSyndicateList()){
				Player p = Bukkit.getPlayerExact(s);
				p.setCompassTarget(plugin.game.getTarget().getLocation());
			}
		}
		if(extraTimeActive){
			if(timeInSeconds > (60 * 5) && plugin.game.world.getTime() >= 18000L){
				plugin.game.world.setTime(18000L);
			}
		}
		if(timeInSeconds == (origin - 300)){
			if(plugin.getConfig().getBoolean("purge.devTool"))
				plugin.getLogger().info("Compass Init");
			for(String s : plugin.game.getSyndicateList()){
				ItemStack c = new ItemStack(Material.COMPASS, 1);
				ItemMeta cm = c.getItemMeta();
				cm.setDisplayName(ChatColor.AQUA + "Location of the Target");
				c.setItemMeta(cm);
				Bukkit.getPlayer(s).getInventory().addItem(c);
				Bukkit.getPlayer(s).sendMessage(ChatColor.GREEN + "You have been given a compass to locate the target!");
			}
		}
		if(timeInSeconds % 60 == 0 && timeInSeconds > 0){
			broadcastTime(true);
			return;
		}
		if(timeInSeconds % 15 == 0 && timeInSeconds > 0){
			broadcastTime(false);
		}
		if(timeInSeconds <= 30 && timeInSeconds > 20){
			broadcastTime(true);
		}
		if(timeInSeconds <= 10 && timeInSeconds > 0){
			broadcastTime(true);
		}
		if(timeInSeconds == 0){
			this.plugin.game.endGame();
		}
	}
	private void broadcastTime(boolean b){
		if(!b == true){
			return;
		}
		if(timeInSeconds % 60 == 0){
			Bukkit.broadcastMessage(plugin.prefix + timeInSeconds / 60 + " minutes remain for the annual Purge!");
			return;
		}
		if(timeInSeconds % 15 == 0){
			Bukkit.broadcastMessage(plugin.prefix + timeInSeconds + " seconds remain for the annual Purge!");
			return;
		}
		if(timeInSeconds <= 10 && timeInSeconds > 00){
			Bukkit.broadcastMessage(plugin.prefix + timeInSeconds + " seconds remain until the annual Purge ends.");
		}
	}
	private void resetTime(){
		origin = plugin.getConfig().getInt("purge.timeInGame");
		timeInSeconds = origin;
	}
	public void setTime(int i){
		this.timeInSeconds = i;
	}
	public void stop(){
		Bukkit.getScheduler().cancelTask(this.taskID);
	}
}
