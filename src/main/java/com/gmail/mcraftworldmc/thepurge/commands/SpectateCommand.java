package com.gmail.mcraftworldmc.thepurge.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.gmail.mcraftworldmc.thepurge.Main;
@SuppressWarnings("deprecation")
public class SpectateCommand implements CommandExecutor{
	private Main plugin;
	public SpectateCommand(Main plugin){
		this.plugin = plugin;
	}
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
		if(!(sender instanceof Player)){
			return false;
		}
		Player p = (Player) sender;
		if(!plugin.game.getSpectatorList().contains(p.getName())){
			p.sendMessage(ChatColor.RED + "You are not a spectator!");
			return true;
		}
		if(args.length == 0){
			p.sendMessage(ChatColor.RED + "Usage: /spectate <name>");
			return true;
		}
		if(Bukkit.getPlayerExact(args[0]) == null){
			p.sendMessage(ChatColor.RED + "That player is not online!");
			return true;
		}
		Player t = Bukkit.getPlayerExact(args[0]);
		if(plugin.game.getSpectatorList().contains(t.getName())){
			p.sendMessage(ChatColor.RED + "You cannot spectate other spectators you spectator!");
			return true;
		}
		p.teleport(t);
		return true;
	}
}
